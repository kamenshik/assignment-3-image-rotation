#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include "util.h"

static int read_file_error_handler(enum read_status read_file_result) {
    switch (read_file_result) {
        case READ_OK:
            break;
        case CREATE_IMAGE_ERROR:
            fprintf(stderr, "Allocate Error");
            return 1;
        case READ_INVALID_SIGNATURE:
            fprintf(stderr, "Signature corrupted");
            return 1;
        case READ_INVALID_HEADER:
            fprintf(stderr, "Header corrupted");
            return 1;
        case READ_INVALID_BITS:
            fprintf(stderr, "Body corrupted");
            return 1;
        default:
            fprintf(stderr, "Unexpected Error");
            return 1;
    }
    return 0;
}

static int handling_closing_file(FILE* file) {
    if (close_file(file) != 0) {
        fprintf(stderr, "Can't close file");
        return 1;
    }
    printf("Close file");
    return 0;
}

int main(int argc, char** argv) {
    if (argc != 3) {
        fprintf(stderr, "Problems with args");
        return 1;
    }
    FILE* in = NULL;
    if (open_file(&in, argv[1], "rb") == OPEN_FAILED) {
        fprintf(stderr, "Can't read from file");
        return 1;
    }
    struct image source = {0};
    const enum read_status read_file_result = from_bmp(in,&source);
    if (handling_closing_file(in) != 0) {
        return 1;
    }
    const int result_read_file_exception_handler = read_file_error_handler(read_file_result);
    if (result_read_file_exception_handler != 0) {
        return result_read_file_exception_handler;
    }
    struct image new_img = {0};
    const enum image_create state = rotate_image(&source,&new_img);
    free_image(&source);
    if (state == CREATE_FAILED) {
        fprintf(stderr, "Allocate Error");
        return 1;
    }

    FILE *out;
    if (open_file(&out,argv[2],"wb") == OPEN_FAILED) {
        fprintf(stderr, "Can't write in file");
        return 1;
    }
    const enum write_status write_result = to_bmp(out,&new_img);
    free_image(&new_img);
    if (handling_closing_file(out) != 0) {
        return 1;
    }
    if (write_result == WRITE_ERROR) {
        printf("Create bmp error");
        return 1;
    }
}
