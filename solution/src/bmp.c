//
// Created by sus on 08.11.22.
//

#include "bmp.h"
#include "image.h"

static uint8_t bmp_calc_padding(uint64_t width) {
    return 4 - (width * sizeof(struct pixel)) % 4;
}

static enum read_status bmp_check_header(const FILE* file, struct bmp_header* header) {
    if (!fread(header, sizeof(struct bmp_header), 1, (FILE*) file)) {
        return READ_INVALID_HEADER;
    }
    if (header->bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }
    if (header->biBitCount != 24 || fseek((FILE*) file, header->bOffBits, SEEK_SET) != 0) {
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum read_status from_bmp(const FILE* file, const struct image *img) {
    if (!file) {return READ_NULL_FILE;}
    if (!img) {return READ_NULL_IMG;}
    struct bmp_header header;
    const enum read_status header_read_status = bmp_check_header(file, &header);
    if (header_read_status != READ_OK) {return header_read_status;}

    enum image_create image_create_status = create_image(
        header.biWidth,
        header.biHeight,
        (struct image*) img
    );
    if (image_create_status == CREATE_FAILED) {return CREATE_IMAGE_ERROR;}

    const uint8_t bmp_padding = bmp_calc_padding(img->width);
    for (uint64_t y = 0; y < img->height; y++) {
        if (fread(
                image_find_pixel(0, y, img),
                sizeof(struct pixel),
                img->width,
                (FILE*) file) != img->width) {return READ_INVALID_BITS;}
        if (fseek(
                (FILE*) file,
                bmp_padding,
                SEEK_CUR) != 0) {return READ_INVALID_BITS;}
    }
    return READ_OK;
}

struct bmp_header bmp_create_header(const uint64_t width, const uint64_t height) {
    const uint32_t image_size = (sizeof(struct pixel) * width + bmp_calc_padding(width)) * height;
    const uint32_t file_size = OFF_BITS_HEADER_SIZE + image_size;
    return (struct bmp_header) {
            .bfType = TYPE,
            .bfileSize = file_size,
            .bfReserved = RESERVED,
            .bOffBits = OFF_BITS_HEADER_SIZE,
            .biSize = HEADER_SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = PLANES,
            .biBitCount = BITCOUNT,
            .biCompression = COM,
            .biSizeImage = image_size,
            .biXPelsPerMeter = PERLX,
            .biYPelsPerMeter = PERLY,
            .biClrUsed = CLRUSED,
            .biClrImportant = CLRIMPORTANT
    };
}

enum write_status to_bmp(const FILE* file, const struct image* img) {
    const struct bmp_header header = bmp_create_header(img->width, img->height);
    if (fwrite(
            &header,
            sizeof(struct bmp_header),
            1,
            (FILE*) file) != 1) {return WRITE_ERROR;}
    const uint8_t bmp_padding = bmp_calc_padding(img->width);
    for (uint64_t y = 0; y < img->height; y++) {
        if (fwrite(
                image_find_pixel(0, y, img),
                sizeof(struct pixel), img->width,
                (FILE*) file) != img->width) {return WRITE_ERROR;}
        for (int i = 0; i < bmp_padding; i++) {
            if (putc(0, (FILE*) file) == EOF) {return WRITE_ERROR;}
        }
    }
    return WRITE_OK;
}
