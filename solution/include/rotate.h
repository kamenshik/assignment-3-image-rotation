//
// Created by sus on 08.11.22.
//

#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include <stdint.h>
#include <stdlib.h>

#include "image.h"

enum image_create rotate_image(const struct image* source, struct image* new_img);

#endif //IMAGE_TRANSFORMER_ROTATE_H
