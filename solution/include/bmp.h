//
// Created by sus on 08.11.22.
//

#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#define BITCOUNT 24
#define CLRIMPORTANT 0
#define CLRUSED 0
#define COM 0
#define HEADER_SIZE 40
#define OFF_BITS_HEADER_SIZE 54
#define PERLX 0
#define PERLY 0
#define PLANES 1
#define RESERVED 0
#define TYPE 0x4D42

#include <stdint.h>
#include <stdio.h>

#include "image.h"
#include "status.h"

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status from_bmp(const FILE* file, const struct image* img);
enum write_status to_bmp(const FILE* file, const struct image* img);
struct bmp_header bmp_create_header(uint64_t width, uint64_t height);

#endif  // IMAGE_TRANSFORMER_BMP_H
