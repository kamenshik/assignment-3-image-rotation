//
// Created by sus on 08.11.22.
//

#ifndef IMAGE_TRANSFORMER_STATUS_H
#define IMAGE_TRANSFORMER_STATUS_H

enum read_status  {
    READ_OK = 0,
    READ_NULL_FILE,
    READ_NULL_IMG,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    CREATE_IMAGE_ERROR,
    /* коды других ошибок  */
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

#endif  // IMAGE_TRANSFORMER_STATUS_H
