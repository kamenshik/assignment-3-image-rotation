//
// Created by sus on 08.11.22.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdint.h>
#include <stdlib.h>

enum image_create {
    CREATE_OK,
    CREATE_FAILED
};
struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};
struct image {
    uint64_t width, height;
    struct pixel* data;
};

enum image_create create_image(uint64_t width, uint64_t height, struct image* img);
void free_image(struct image* img);
struct pixel* image_find_pixel(uint64_t x, uint64_t y, const struct image* img);

#endif  // IMAGE_TRANSFORMER_IMAGE_H
